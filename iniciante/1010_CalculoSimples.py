peca1,numPecas1,valorUnit1 = map(float,input().split())
peca2,numPecas2,valorUnit2 = map(float,input().split())

valorTotal = numPecas1 * valorUnit1 + numPecas2 * valorUnit2

print('VALOR A PAGAR: R$ {:.2f}'.format(valorTotal))
