num = int(input())
for c in range(1, 11):
    print('{} x {:2} = {}'.format(c, num, num*c))
