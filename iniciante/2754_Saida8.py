n1 = 234.345
n2 = 45.698

print('{:.6f} - {:.6f}'.format(n1, n2))
print('{:.0f} - {:.0f}'.format(n1, n2))
print('{:.1f} - {:.1f}'.format(n1, n2))
print('{:.2f} - {:.2f}'.format(n1, n2))
print('{:.3f} - {:.3f}'.format(n1, n2))
print('{:e} - {:e}'.format(n1, n2))
print('{:E} - {:E}'.format(n1, n2))
print('{} - {}'.format(n1, n2))
print('{} - {}'.format(n1, n2))
